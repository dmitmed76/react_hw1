import React from 'react';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal.jsx';
import './App.css';

export default class App extends React.Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  };

  openFirstModal = () => {
    this.setState({ isFirstModalOpen: true });
  };

  openSecondModal = () => {
    this.setState({ isSecondModalOpen: true });
  };

  closeModal = () => {
    this.setState({ 
      isFirstModalOpen: false,
      isSecondModalOpen: false  
    });
  };

  handleOutsideClick = (event) => {
    // Перевіряємо, чи клік був здійснений за межами модального вікна
    if (event.currentTarget === event.target) {
      //Якщо так, то додаємо код для закриття модального вікна
      this.setState({ 
        isFirstModalOpen: false,
        isSecondModalOpen: false  
      });
    }
  };

  render() {
    return (
      <div className="btn-wrapper" >
        <Button
          optionalСlassName="btn-primary"
          backgroundColor="darkgrey"
          margin="50px"
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          optionalСlassName="btn-secondary"
          backgroundColor="blue"
          text="Open second modal"
          onClick={this.openSecondModal}
        />

        {this.state.isFirstModalOpen && (
          <Modal
            header="Modal window one"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutsideClick={this.handleOutsideClick}
            text="This is first modal window"
            actions={
              <div className="button-container">
                  <Button 
                  className="btn" 
                  onClick={this.closeModal} 
                  text="Ok" 
                  />
                  <Button 
                  className="btn" 
                  onClick={this.closeModal}
                  text="Cancel"
                  />
              </div>
            }
          />
        )}

        {this.state.isSecondModalOpen && (
          <Modal
            header="Home Work 1 - MODAL-WINDOW"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutsideClick={this.handleOutsideClick}
            text="Прошу вас оцінити роботу?"
            actions={
              <div className="button-container">
                <Button 
                  optionalСlassName="btn-primary" 
                  backgroundColor="black"
                  onClick={this.closeModal}
                  text="КРУТО!"
                />
                <Button
                  optionalСlassName="btn-secondary" 
                  backgroundColor="white" 
                  color = "black"
                  onClick={this.closeModal}
                  text="Fine"
                />
                <Button 
                  onClick={this.closeModal}
                  text="!-!"
                />
              </div>
            }
          />
        )}
      </div>
    );
  }
}
